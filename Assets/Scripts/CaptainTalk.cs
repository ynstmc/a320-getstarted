﻿using UnityEngine;
using UnityEngine.UI;

public class CaptainTalk : MonoBehaviour{
	public Text IgnoreThis;
	public Text CaptainT;
	private bool isAnnouncementClicked = false;
	private string captainText;
	private int showingChar = 20;
	private int index = 0;
	private int flagText = 0;
	
	public  Image spect;
	private Image[] images;
	private int flagImage = 0;


	void Start (){
		captainText = IgnoreThis.text;
		images = spect.GetComponentsInChildren<Image>();
	}
	
	void Update () {
		if(flagImage == 8 ){
			if(isAnnouncementClicked)
				scaleImages();
			flagImage = 0;
		}

		++flagImage;
		
		
		if(flagText == 8){
			if(isAnnouncementClicked)
			showText();
			flagText = 0;
		}

		++flagText;
		//Debug.Log("flagText :"+flagText+" 	flagImage :"+flagImage);
	}

	public void announcementClicked(){
		//Controller.steps.Dequeue().SetBool("showNextStep",false);
		//Controller.showNextStepTime = Controller.DEFAULT_TIME;
		isAnnouncementClicked = true;
	}
	public void scaleImages(){
		
		for (int i = 0; i <= 51; ++i){
			float randNumber = Random.Range(0, 20);
			images[i].rectTransform.sizeDelta = new Vector2(1.5f,randNumber);
		}
	
	}
	public void showText(){
		if(index < captainText.Length-showingChar){
			CaptainT.text = captainText.Substring(index, showingChar);
			++index;
		}
		else{
			isAnnouncementClicked = false;
		}
	}
}
