﻿
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LongClickImage : MonoBehaviour ,IPointerDownHandler,IPointerUpHandler{

	private bool pointerDown;
	private float pointerDownTimer;
	public float requiredHoldTime;
	public Image fillImage;
	
	void Update () {
		if (pointerDown){
			if(fillImage.fillAmount == 1)
				Debug.Log("FULL");
			else{
				pointerDownTimer += Time.deltaTime;
				fillImage.fillAmount = pointerDownTimer / requiredHoldTime;
				Controller.showNextStepTime = Controller.DEFAULT_TIME;
			}
			
		}
		else{
			Reset();
		}
	}

	public void OnPointerDown(PointerEventData eventData){
		pointerDown = true;
	}

	public void OnPointerUp(PointerEventData eventData){
		pointerDown = false;

	}
	private void Reset(){

		pointerDown = false;
		if (fillImage.fillAmount != 1.0 ){
			if(fillImage.fillAmount>=0 && fillImage.fillAmount<=0.05){
				pointerDownTimer = 0;
				//Debug.Log("IFF");
			}
			else{
				pointerDownTimer -= Time.deltaTime;
				//Debug.Log("ELSE");
				Controller.showNextStepTime = Controller.DEFAULT_TIME;
			}
		}
		fillImage.fillAmount = pointerDownTimer / requiredHoldTime;
	}
}
