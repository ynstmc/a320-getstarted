﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class leverMotion : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler{
    private Vector3 startPos;
    public Image Ignore1;
    public Image Ignore2;
    
    public void OnDrag(PointerEventData eventData){
        if(Input.mousePosition.y <= Ignore1.transform.position.y)
            transform.position = new Vector3(startPos.x,Ignore1.transform.position.y,startPos.z);
        else if(Input.mousePosition.y >= Ignore2.transform.position.y)
            transform.position = new Vector3(startPos.x,Ignore2.transform.position.y,startPos.z);
        else
            transform.position = new Vector3(startPos.x,Input.mousePosition.y,startPos.z);
           
        Debug.Log("Mouse Pos : "+Input.mousePosition.y + "     Start Pos : " + startPos.y);
    }

    public void OnEndDrag(PointerEventData eventData){

        if(Input.mousePosition.y <= Ignore1.transform.position.y)
            transform.position = new Vector3(startPos.x,Ignore1.transform.position.y,startPos.z);
        else if(Input.mousePosition.y >= Ignore2.transform.position.y)
            transform.position = new Vector3(startPos.x,Ignore2.transform.position.y,startPos.z);
        else
            transform.position = new Vector3(startPos.x,Input.mousePosition.y,startPos.z);
    }

    public void OnBeginDrag(PointerEventData eventData){
        startPos = transform.position;
    }
}
