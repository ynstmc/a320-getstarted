﻿using UnityEngine;
using UnityEngine.UI;

public class ENG : MonoBehaviour{
	public Image ENGInside;
	public Button startBtn;
	public Image tick;
	private float percentTick;
	private Vector3 rotate = new Vector3(0,0,0);
	public bool start = false;
	public int speed = 60;

	public Animator showNextStep;
	private bool flag = false;
	void Update () {
		if(start){
			rotateImage();
			percentTick += 5;
			tick.fillAmount = percentTick / 100;
		}
		if(speed<=250 && flag )
			speed += 1;
	}

	public void startClicked(){
		Controller.steps.Dequeue().SetBool("showNextStep",false);
		Controller.showNextStepTime = Controller.DEFAULT_TIME;
		//showStep.SetBool("isDisplayed",false);
		start = true;
		startBtn.interactable = false;
		flag = true;
		
	}

	public void rotateImage(){
		ENGInside.transform.position += (transform.rotation*rotate);
		ENGInside.transform.rotation *= Quaternion.AngleAxis(speed*Time.deltaTime, -Vector3.forward);
		ENGInside.transform.position -= (transform.rotation*rotate);
	}
}
