﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Controller : MonoBehaviour{
	public const float DEFAULT_TIME = 5;
	public static float showNextStepTime = DEFAULT_TIME;
	public static Queue<Animator> steps;
	
	public Battery bat1 ;
	public Battery bat2 ;
	public Image batteriesReady;
	private float percentReadyBatteries;
	
	public APU apu;
	public Image APUReady1;
	public Image APUReady2;
	private float percentAPUReady;
	
	public ENG eng1;
	public ENG eng2;
	
	public ADIRS Adirs1;
	public ADIRS Adirs2;
	public ADIRS Adirs3;
	
	public Image blurTop;
	public Image blurCenter;
	public Image blurBottom;

	public Animator step1;
	public Animator step2;
	public Animator step3;
	public Animator step4;

	public Animator popUpMenu1;

	private int flag = 0;
	
	
	/*public  Animator showNextStep7;
	public  Animator showNextStep8;
	public  Animator showNextStep9;
	public  Animator showNextStep10;
	public  Animator showNextStep11;*/

	void Start (){
		
		steps = new Queue<Animator>();
		steps.Enqueue(bat1.showNextStep);
		steps.Enqueue(bat2.showNextStep);
		steps.Enqueue(apu.showNextStep);
		steps.Enqueue(eng1.showNextStep);
		steps.Enqueue(eng2.showNextStep);
		steps.Enqueue(Adirs1.showNextStep);
		steps.Enqueue(Adirs2.showNextStep);
		steps.Enqueue(Adirs3.showNextStep);
		/*steps.Enqueue(showNextStep7);
		steps.Enqueue(showNextStep8);
		steps.Enqueue(showNextStep9);
		steps.Enqueue(showNextStep10);
		steps.Enqueue(showNextStep11);*/
		
		step1.SetBool("isDisplayed",true);
		//bat1.showStep.SetBool("isDisplayed",true);
		
		bat2.batOn.interactable = false;
		bat2.batOff.interactable = false;
		bat2.batFillingBack.GetComponent<LongClickImage>().enabled = false;
		
		apu.btnTest.interactable = false;
		apu.btnStart.interactable = false;
		
		eng1.startBtn.interactable = false;
		eng2.startBtn.interactable = false;
		
		Adirs2.off.interactable = false;
		Adirs2.nav.interactable = false;
		Adirs2.att.interactable = false;
		Adirs2.switingImage.GetComponent<Button>().interactable = false;
			
		Adirs3.off.interactable = false;
		Adirs3.nav.interactable = false;
		Adirs3.att.interactable = false;
		Adirs3.switingImage.GetComponent<Button>().interactable = false;
		
		//nextWindow.interactable = false;
	}
	void Update (){
		showNextStepTime -= Time.deltaTime;
		if (showNextStepTime < 0 && steps.Count != 0){
			steps.Peek().SetBool("showNextStep",true);
			showNextStepTime = DEFAULT_TIME;
		}
		
		if (bat1.batFillingBack.fillAmount == 1 && flag == 0){
			if ( bat1.batButtonImage.sprite!= bat1.on){
				steps.Dequeue().SetBool("showNextStep",false);
				
				bat1.batFillingBack.fillAmount = 1;
				bat1.batButtonImage.sprite = bat1.on;
				bat1.batOn.GetComponentInChildren<Outline>().enabled = true;
				bat1.batOff.GetComponentInChildren<Outline>().enabled = false;
			}

			bat2.batOn.interactable = true;
			bat2.batOff.interactable = true;
			bat2.batFillingBack.GetComponent<LongClickImage>().enabled = true;
			
			bat1.batOn.interactable = false;
			bat1.batOff.interactable = false;
			++flag;
		}else if (bat2.batFillingBack.fillAmount == 1 && percentReadyBatteries<=100){
			
			if ( bat2.batButtonImage.sprite!= bat1.on){
				steps.Dequeue().SetBool("showNextStep",false);
				
				bat2.batFillingBack.fillAmount = 1;
				bat2.batButtonImage.sprite = bat1.on;
				bat2.batOn.GetComponentInChildren<Outline>().enabled = true;
				bat2.batOff.GetComponentInChildren<Outline>().enabled = false;
			}
			
			if(percentReadyBatteries == 0){
				bat2.batOn.interactable = false;
				bat2.batOff.interactable = false;
			}
			else if (percentReadyBatteries == 75){
				blurCenter.enabled = false;
			}
			else if (percentReadyBatteries == 100){
				apu.btnTest.interactable = true;
				
				step1.SetBool("isDisplayed",false);
				step2.SetBool("isDisplayed",true);

			}
			if(bat2.batReady.fillAmount==1){
				showNextStepTime = DEFAULT_TIME;
				percentReadyBatteries += 1;
				batteriesReady.fillAmount = percentReadyBatteries / 100;
			}
		}

		if (apu.percentTick > 100 && percentAPUReady<=100){
			percentAPUReady += 1;
			APUReady1.fillAmount = percentAPUReady / 100;
			APUReady2.fillAmount = percentAPUReady / 100;
			
			showNextStepTime = DEFAULT_TIME;
			
			if (percentAPUReady == 100){
				eng1.startBtn.interactable = true;
				
				
				step2.SetBool("isDisplayed",false);
				step3.SetBool("isDisplayed",true);
			}	
		}
		
		if(eng1.start && flag == 1){
			eng2.startBtn.interactable = true;
			++flag;
		}else if (eng1.start && eng2.start && flag == 2){
			
			step3.SetBool("isDisplayed",false);
			blurBottom.enabled = false;
			step4.SetBool("isDisplayed",true);
			//Adirs1.showStep.SetBool("isDisplayed",true);
			++flag;
		}
		
		if(Adirs1.clicked){	
			//steps.Dequeue().SetBool("showNextStep",false);
			//showNextStepTime = DEFAULT_TIME;

			Adirs2.off.interactable = true;
			Adirs2.nav.interactable = true;
			Adirs2.att.interactable = true;
			Adirs2.switingImage.GetComponent<Button>().interactable = true;

			Adirs1.clicked = false;
		}else if(Adirs2.clicked){
			//steps.Dequeue().SetBool("showNextStep",false);
			//showNextStepTime = DEFAULT_TIME;
			
			Adirs3.off.interactable = true;
			Adirs3.nav.interactable = true;
			Adirs3.att.interactable = true;
			Adirs3.switingImage.GetComponent<Button>().interactable = true;

			Adirs2.clicked = false;
		}else if (Adirs3.clicked){
			//steps.Dequeue().SetBool("showNextStep",false);
			//showNextStepTime = DEFAULT_TIME;

			Adirs3.clicked = false;
		}else if (Adirs1.BatOn.GetComponent<Outline>().enabled &&
		          Adirs2.BatOn.GetComponent<Outline>().enabled &&
		          Adirs3.BatOn.GetComponent<Outline>().enabled){
			
			blurTop.enabled = true;
			blurCenter.enabled = true;
			blurBottom.enabled = true;
			popUpMenu1.SetBool("popUpMenu",true);
			/*if(flag == 1)
				showNextStepTime = DEFAULT_TIME;*/
			
		}
			
			
	}

	public void DisableBoolAnimator(Animator anim){
		anim.SetBool("IsDisplayed",true);
		step4.SetBool("isDisplayed",false);
		//++flag;
	}
	
	public void EnableBoolAnimator(Animator anim){
		anim.SetBool("IsDisplayed",true);
		step4.SetBool("isDisplayed",false);
		//++flag;
	}
	
	public void noSmoking(Image mark){
		mark.enabled = true;
		
		/*steps.Dequeue().SetBool("showNextStep",false);
		showNextStepTime = DEFAULT_TIME;*/
	}

	public void electronicDevice(Image mark){
		mark.enabled = true;
		
		/*steps.Dequeue().SetBool("showNextStep",false);
		showNextStepTime = DEFAULT_TIME;*/
	}

	public void seatBeltAnimLeft(Animator animLeft){
		animLeft.SetBool("slideRight",true);
		
		/*steps.Dequeue().SetBool("showNextStep",false);
		showNextStepTime = DEFAULT_TIME;*/
	}
	
	public void seatBeltAnimRight(Animator animRight){
		animRight.SetBool("slideLeft",true);
		
		/*steps.Dequeue().SetBool("showNextStep",false);
		showNextStepTime = DEFAULT_TIME;*/
	}

	public void thridPanelDisplay(Animator anim){
		anim.SetBool("IsDisplayed",true);
	}
	public void secondPanelDisable(Animator anim){
		anim.SetBool("IsDisplayed",false);
	}
}
