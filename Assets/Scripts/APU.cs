﻿using UnityEngine;
using UnityEngine.UI;

public class APU : MonoBehaviour {
	
	public Button btnTest;
	public Button btnStart;
	public Text Fire;
	public Text Push;
	public Image APUInsideBack;
	public Image APUOutsideBack;
	public Image tick;
	public Text apu;
	
	private float percentApuInside;
	private float percentApuOutside;
	public float percentTick;
	private int flag = 0;
	
	public Animator showNextStep;
	
	void Start (){
		percentTick = 0;
		percentApuInside = 0;
		percentApuOutside = 0;
	}
	
	
	void Update (){

		if (APUInsideBack.fillAmount == 1 && flag == 0){
			APUTestClicked();
			percentApuInside = 101;
			++flag;
		}
		if (Push.GetComponent<Outline>().enabled && percentApuInside<=100)
			fillAPUInside();
		if (percentApuOutside > 0 && percentApuOutside <= 100)
			APUStartClicked();
		if (APUOutsideBack.fillAmount == 1 && percentTick<=100){
			percentTick += 5;
			tick.fillAmount = percentTick / 100;
			if (percentTick == 100)
				apu.GetComponent<Outline>().enabled = true;
		}
	}
	public void APUTestClicked(){
		
		Controller.steps.Peek().SetBool("showNextStep",false);
		Controller.showNextStepTime = Controller.DEFAULT_TIME;
		
		//step1.SetBool("isDisplayed",false);
		
		Fire.GetComponent<Outline>().enabled = true;
		Push.GetComponent<Outline>().enabled = true;
		btnStart.interactable = true;
		btnTest.interactable = false;

		APUInsideBack.GetComponent<LongClickImage>().enabled = false;
		//step2.SetBool("isDisplayed",true);
	}
	
	private void APUStartClicked(){
		percentApuOutside += 1;
		APUOutsideBack.fillAmount = percentApuOutside / 100;
		if (flag == 1){
			Controller.steps.Dequeue().SetBool("showNextStep",false);
			Controller.showNextStepTime = Controller.DEFAULT_TIME;
			++flag;
		}
		btnStart.interactable = false;
		//step2.SetBool("isDisplayed",false);
	}
	private void fillAPUInside(){
		percentApuInside += 1;
		APUInsideBack.fillAmount = percentApuInside / 100;
	}
}
