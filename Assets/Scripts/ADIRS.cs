﻿using UnityEngine;
using UnityEngine.UI;

public class ADIRS : MonoBehaviour{

	public Image switingImage;
	public Text Alingt;
	public Text BatOn;
	public Button off;
	public Button nav;
	public Button att;
	public Image IRready;
	public Image ADIRSready;
	public Image tick;

	public float percentIRready;
	private float percentADIRSready;
	private float percentTick;

	public Animator showNextStep;
	
	private string switchBtnState = "off";
	public bool clicked = false;
	void Update () {
		if (switchBtnState == "nav" && percentIRready <=100){
			percentIRready += 1;
			IRready.fillAmount = percentIRready / 100;
			if (percentIRready == 100){
				Alingt.color = Color.white;
				Alingt.GetComponent<Outline>().enabled = true;
			}
		}

		if (IRready.fillAmount == 1 && percentADIRSready<=100) {
			percentADIRSready += 1;
			ADIRSready.fillAmount = percentADIRSready / 100;
		}

		if (ADIRSready.fillAmount == 1 && percentTick<=100){
			BatOn.GetComponent<Outline>().enabled = true;
			percentTick += 5;
			tick.fillAmount = percentTick / 100;
		}
	}

	public void offClicked(){
		if(switchBtnState == "nav")
			switingImage.rectTransform.Rotate(new Vector3(0,0,90));
		else if(switchBtnState == "att")
			switingImage.rectTransform.Rotate(new Vector3(0,0,180));
		
		switchBtnState = "off";
	}

	public void navClicked(){
		
		Controller.steps.Dequeue().SetBool("showNextStep",false);
		Controller.showNextStepTime = Controller.DEFAULT_TIME;
		
		if(switchBtnState == "off")
			switingImage.rectTransform.Rotate(new Vector3(0,0,-90));
		else if(switchBtnState == "att")
			switingImage.rectTransform.Rotate(new Vector3(0,0,90));

		off.interactable = false;
		att.interactable = false;
		
		switchBtnState = "nav";
		clicked = true;

		switingImage.GetComponent<Button>().interactable = false;
	}

	public void attClicked(){
		if(switchBtnState == "off")
			switingImage.rectTransform.Rotate(new Vector3(0,0,-180));
		else if(switchBtnState == "nav")
			switingImage.rectTransform.Rotate(new Vector3(0,0,-90));
		
		switchBtnState = "att";
	}
}
