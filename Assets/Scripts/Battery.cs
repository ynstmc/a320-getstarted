﻿using UnityEngine;
using UnityEngine.UI;

public class Battery : MonoBehaviour{

	public Sprite on;
	
	public Text batText;
	public Image batFillingBack;
	public Image batButtonImage;
	public Button batOn;
	public Button batOff;
	public Image batTick;
	public Image batReady;

	public float percentReady;
	private float percentFillingBack;
	private float percentTick;
	
	public Animator showNextStep;
	
	private int percent;
	
	void Start (){
		percentFillingBack = 0;
		percentTick = 0;
	}
	
	void Update () {
		if(batButtonImage.sprite == on){
			if (batFillingBack.fillAmount == 1)
				percentFillingBack = 100;
			fillBat();
		}
		
	}
	public void batOnClicked(){
//		showStep.SetBool("isDisplayed",false);
		Controller.steps.Dequeue().SetBool("showNextStep",false);
		Controller.showNextStepTime = Controller.DEFAULT_TIME;
		
		batFillingBack.GetComponent<LongClickImage>().enabled = false;
		
		batButtonImage.sprite = on;
		batOn.GetComponentInChildren<Outline>().enabled = true;
		batOff.GetComponentInChildren<Outline>().enabled = false;

		batOn.interactable = false;
		batOff.interactable = false;
	}
	private void fillBat(){
		if (percentFillingBack == 100){
			//Debug.Log("Tick");
			if (percentTick <= 100){
				percentTick += 5;
				batTick.fillAmount = percentTick / 100;
				//Debug.Log("Ready");
			}
			
			if (percentTick == 100){
				//Debug.Log("OutLine");
				percentTick += 1;
				batText.GetComponent<Outline>().enabled = true;
			}

			if (batText.GetComponent<Outline>().enabled && percentReady<=100){
				percentReady += 1;
				batReady.fillAmount = percentReady / 100;
			}
		}
		else{
			percentFillingBack += 1;
			batFillingBack.fillAmount = percentFillingBack / 100;
			//Debug.Log("Bat is filling");
		}	
	}
}
